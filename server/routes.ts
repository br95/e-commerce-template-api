import { Application } from "express";
import productsRouter from "./api/controllers/products/product.router";
import adminRouter from "./api/controllers/admin/admin.router";
import authHandler from "./api/middlewares/auth/auth.handler";

export default function routes(app: Application): void {
  app.use("/api/v1/products", productsRouter);
  app.use("/api/v1/protected/admin/", [authHandler], adminRouter);
}
