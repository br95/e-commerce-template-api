import { Request, Response, NextFunction } from "express";
import jwt_decode from "jwt-decode";
import l from "../../../common/logger";

const ADMIN = "admin";
const USER = "user";

const credentials = {
  client_id: process.env.KEYCLOAK_CLIENT_ID,
  grant_type: process.env.KEYCLOAK_GRANT_TYPE,
  client_secret: process.env.KEYCLOAK_SECRET,
};

export default function authHandler(
  req: Request,
  res: Response,
  next: NextFunction
) {
  //Try to validate the token and get data
  const isRouteAdmin = <boolean>req.baseUrl.includes(ADMIN);
  const isRouteUser = <boolean>req.baseUrl.includes(USER);
  if (isRouteAdmin || isRouteUser) {
    const auth = <string>req.headers.authorization;
    if (!auth) throw new Error("This endpoint requires authorization!");
    try {
      let jwtPayload;
      const token = <string>auth.split(" ")[1];
      if (!token) throw new Error("There is not token in the request!");
      jwtPayload = jwt_decode(token);
      // Ukoliko je Admin ruta
      if (isRouteAdmin) {
        if (!checkRole(jwtPayload, true))
          throw new Error("User is not in the 'Admin' role!");
      }
      // Ukoliko je korisnicka ruta
      if (isRouteUser) {
        if (!checkRole(jwtPayload, false))
          throw new Error("User is not authenticated!");
      }
      // Ukoliko je korisnicka ruta
      l.info(`Endpoint: ${req.baseUrl}`);
      res.locals.jwtPayload = jwtPayload;
    } catch (error) {
      //If token is not valid, respond with 401 (unauthorized)
      res.status(401).send({ error: error.message });
      return;
    }
  }
  //Call the next middleware or controller
  next();
}

const checkRole = (jwtObj: Object, isAdmin: boolean): boolean => {
  const roles = <Array<String>>(
    jwtObj["resource_access"][credentials.client_id].roles
  );
  if (isAdmin) return roles.some((role) => role === "Admin");
  return roles.some((role) => role === "User");
};
