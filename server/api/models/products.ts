import mongoose from "mongoose";
import sequence from "mongoose-sequence";

const AutoIncrement = sequence(mongoose);

export interface IProductModel extends mongoose.Document {
  id: number;
  name: string;
  price: number;
  tax: number;
  description: number;
  image: string;
  gender: String;
}

const productSchema = new mongoose.Schema(
  {
    id: { type: Number, unique: true },
    name: String,
    price: Number,
    tax: Number,
    description: String,
    image: String,
    gender: String,
  },
  {
    collection: "products",
  }
);

productSchema.plugin(AutoIncrement, { inc_field: "id" });

export const Product = mongoose.model<IProductModel>("products", productSchema);
