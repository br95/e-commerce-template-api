import { Request, Response, NextFunction } from "express";
import ProductService from "../../services/product.service";

export class AdminController {
  async create(req: Request, res: Response, next: NextFunction) {
    try {
      const doc = await ProductService.create(req.body);
      return res.status(201).location(`/api/v1/products/${doc.id}`).end();
    } catch (err) {
      return next(err);
    }
  }

  async update(req: Request, res: Response, next: NextFunction) {
    try {
      const doc = await ProductService.update(
        parseInt(req.params.id),
        req.body
      );
      return res.status(204).location(`/api/v1/products/${doc.id}`).end();
    } catch (err) {
      return next(err);
    }
  }
}

export default new AdminController();
