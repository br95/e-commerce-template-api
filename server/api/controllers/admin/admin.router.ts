import express from "express";
import adminController from "./admin.controller";

export default express
  .Router()
  .post("/products/", adminController.create)
  .put("/products/:id", adminController.update);
