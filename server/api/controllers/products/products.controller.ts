import ProductService from "../../services/product.service";
import { Request, Response, NextFunction } from "express";

export class ProductController {
  async getAll(req: Request, res: Response, next: NextFunction) {
    try {
      const docs = await ProductService.getAll();
      return res.status(200).json(docs);
    } catch (err) {
      return next(err);
    }
  }

  async getById(req: Request, res: Response, next: NextFunction) {
    try {
      const doc = await ProductService.getById(parseInt(req.params.id));
      if (doc) {
        return res.status(200).json(doc);
      }
      const errors = [{ message: "Example not found" }];
      return res.status(404).json({ errors });
    } catch (err) {
      return next(err);
    }
  }
}
export default new ProductController();
