import express from "express";
import productController from "./products.controller";
export default express
  .Router()
  .get("/", productController.getAll)
  .get("/:id", productController.getById);
