import l from "../../common/logger";

import { Product, IProductModel } from "../models/products";

export class ProductService {
  async getAll(): Promise<IProductModel[]> {
    l.info("Getting all products");
    const products = (await Product.find(
      null,
      "-_id -__v"
    ).lean()) as IProductModel[];
    return products;
  }

  async getById(id: number): Promise<IProductModel> {
    l.info(`Getting product with ${id}`);
    const product = (await Product.findOne(
      { id: id },
      "-_id -__v"
    ).lean()) as IProductModel;
    return product;
  }

  async create(data: IProductModel): Promise<IProductModel> {
    l.info(`Created product with data ${{ ...data }}`);
    const product = new Product(data);
    const doc = (await product.save()) as IProductModel;
    return doc;
  }

  async update(id: Number, data: IProductModel): Promise<IProductModel> {
    l.info(`Updated product with data ${{ ...data }}`);
    const doc = (await Product.updateOne(
      { id: id },
      { $set: { ...data } }
    )) as IProductModel;
    return doc;
  }
}

export default new ProductService();
