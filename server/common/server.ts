import express from "express";
import { Application } from "express";
import path from "path";
import http from "http";
import os from "os";
import cookieParser from "cookie-parser";
import installValidator from "./openapi";
import l from "./logger";
import morgan from "morgan";
import { IDatabase } from "./database";
import session from "express-session";
import Keycloak from "keycloak-connect";

const app = express();

const memoryStore = new session.MemoryStore();
export const keycloak = new Keycloak({ store: memoryStore });

export default class ExpressServer {
  constructor() {
    const root = path.normalize(__dirname + "/../..");
    app.set("appPath", root + "client");
    app.use(morgan("dev"));
    app.use(express.json({ limit: process.env.REQUEST_LIMIT || "100kb" }));
    app.use(
      express.urlencoded({
        extended: true,
        limit: process.env.REQUEST_LIMIT || "100kb",
      })
    );
    app.use(cookieParser(process.env.SESSION_SECRET));
    app.use(express.static(`${root}/public`));
    // Keycloak middleware setup
    app.use(
      session({
        secret: "dc0c7f89-93aa-4d06-a55a-8f40355a0e1c",
        resave: false,
        saveUninitialized: true,
        store: memoryStore,
      })
    );
    app.use(keycloak.middleware());
  }

  router(routes: (app: Application) => void): ExpressServer {
    installValidator(app, routes);
    return this;
  }

  database(db: IDatabase): ExpressServer {
    db.init();
    return this;
  }

  listen(p: string | number = process.env.PORT): Application {
    const welcome = (port) => () =>
      l.info(
        `up and running in ${
          process.env.NODE_ENV || "development"
        } @: ${os.hostname()} on port: ${port}`
      );
    http.createServer(app).listen(p, welcome(p));
    return app;
  }
}
