import "mocha";
import { expect } from "chai";
import request from "supertest";
import Server from "../server";

describe("Products", () => {
  it("Should add a new product", () =>
    request(Server)
      .post("/api/v1/products")
      .send({
        name: "Test",
        price: 9.99,
        tax: 9,
        description: "TEST DESCRIPTION",
        image:
          "https://i8.amplience.net/i/Lindex/7953567_766_PS_F/blue-hanna-wide-high-waist-jeans-with-cropped-leg?$fmtJpg$&$cache$&$crop$&$scaleFit$&$productDetailSwiper$&vw=600",
        gender: "Female",
      })
      .expect(201));

  it("Should update a product", () =>
    request(Server)
      .put("/api/v1/products/1")
      .send({
        name: "Test update",
        price: 99.99,
        tax: 99,
        description: "TEST DESCRIPTION UPDATE",
        image:
          "https://i8.amplience.net/i/Lindex/7953567_766_PS_F/blue-hanna-wide-high-waist-jeans-with-cropped-leg?$fmtJpg$&$cache$&$crop$&$scaleFit$&$productDetailSwiper$&vw=600",
        gender: "Male",
      })
      .expect(204));

  it("should get a product by id", () =>
    request(Server)
      .get("/api/v1/products/1")
      .expect("Content-Type", /json/)
      .then((r) => {
        expect(r.body).to.be.an("object");
        expect({ ...r.body }).to.include({
          name: "Test update",
          price: 99.99,
          tax: 99,
          description: "TEST DESCRIPTION UPDATE",
          image:
            "https://i8.amplience.net/i/Lindex/7953567_766_PS_F/blue-hanna-wide-high-waist-jeans-with-cropped-leg?$fmtJpg$&$cache$&$crop$&$scaleFit$&$productDetailSwiper$&vw=600",
          gender: "Male",
        });
      }));

  it("should get all examples", () =>
    request(Server)
      .get("/api/v1/products")
      .expect("Content-Type", /json/)
      .then((r) => {
        expect(r.body).to.be.an("array").of.length(1);
      }));
});
